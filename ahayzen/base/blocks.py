#!/usr/bin/python3
"""
This file is part of ahayzen.com

Copyright (C) 2016, 2018
    Andrew Hayzen <ahayzen@gmail.com>

ahayzen.com is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

ahayzen.com is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with ahayzen.com.  If not, see <https://www.gnu.org/licenses/>.
"""
from django import forms
from django.utils.encoding import force_text
from wagtail.core.blocks import (
    CharBlock, PageChooserBlock, RawHTMLBlock, RichTextBlock,
    StreamBlock, URLBlock
)
from wagtail.core.blocks.field_block import TextBlock, FieldBlock, \
    BooleanBlock
from wagtail.core.blocks.list_block import ListBlock
from wagtail.core.blocks.struct_block import StructBlock
from wagtail.documents.blocks import DocumentChooserBlock
from wagtail.embeds.blocks import EmbedBlock
from wagtail.images.blocks import ImageChooserBlock

from wagtail.contrib.table_block.blocks import TableBlock

# import markdown


# class MarkdownBlock(TextBlock):
#     def render_basic(self, value):
#         return markdown.markdown(value)

class BulletListBlock(StructBlock):
    items = ListBlock(CharBlock(label="Item"))

    class Meta:
        icon = "list-ul"
        template = "base/list_ul.html"


class NumberListBlock(StructBlock):
    items = ListBlock(CharBlock(label="Item"))

    class Meta:
        icon = "list-ol"
        template = "base/list_ol.html"


class HrBlock(StructBlock):
    full_width = BooleanBlock(
        help_text=(
                "Should the horizontal rule span the full width of the"
                "page, or content"
            ),
        required=False
    )

    class Meta:
        icon = "horizontalrule"
        template = "base/hr.html"


# FIXME: Use inbuilt wagtail IntegerBlock when it is available
# https://github.com/torchbox/wagtail/pull/1554
class IntegerBlock(FieldBlock):
    def __init__(self, required=True, help_text=None, **kwargs):
        self.field = forms.IntegerField(
            required=required,
            help_text=help_text,
        )
        super(IntegerBlock, self).__init__(**kwargs)

    def get_searchable_content(self, value):
        return [force_text(value)]


class LatestBlogEntries(StructBlock):
    number = CharBlock(required=False,
                       help_text="Amount of blog entries to show")

    class Meta:
        icon = "pick"


class ExternalLinkBlock(StructBlock):
    title = CharBlock(required=True)
    url = URLBlock(required=True)
    subtitle = TextBlock(required=True, max_length=250)
    image = ImageChooserBlock(required=False)

    class Meta:
        icon = "site"


# If these change remember to update
# - ahayzen/templates/includes/streamfield.html
# - ahayzen/doc/templates/doc/doc_page_markdown_view.html
class AhayzenStreamBlock(StreamBlock):
    heading = CharBlock(form_classname="full title", icon="title")
    paragraph = RichTextBlock(icon="pilcrow")

    bullet_list = BulletListBlock(icon="list-ul")
    numbered_list = NumberListBlock(icon="list-ol")

    hr = HrBlock(icon="horizontalrule")

    table = TableBlock(icon="table")

    external_link = ExternalLinkBlock(icon="site")
    link = PageChooserBlock(icon="link")

    image = ImageChooserBlock(icon="image")
    embed = EmbedBlock(icon="media")
    document = DocumentChooserBlock(icon="doc-empty")

    code = TextBlock(icon="code")
    raw_html = RawHTMLBlock(icon="code")
    # markdown = MarkdownBlock(icon="code")


class AhayzenStreamLinkBlock(StreamBlock):
    link = PageChooserBlock(icon="link")
    external_link = ExternalLinkBlock(icon="site")


class AhayzenStreamHomeBlock(AhayzenStreamBlock):
    latest_block_entries = LatestBlogEntries(icon="pick")
