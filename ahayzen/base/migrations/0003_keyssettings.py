# -*- coding: utf-8 -*-
# Generated by Django 1.10.8 on 2019-03-24 20:56
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('wagtailcore', '0040_page_draft_title'),
        ('base', '0002_footerlink'),
    ]

    operations = [
        migrations.CreateModel(
            name='KeysSettings',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('google_site_verification_code', models.CharField(blank=True, default='', help_text='Google Site Verification Code', max_length=255)),
                ('recaptcha_public_key', models.CharField(blank=True, default='', help_text='ReCaptcha public key', max_length=255)),
                ('recaptcha_private_key', models.CharField(blank=True, default='', help_text='ReCaptcha private key', max_length=255)),
                ('site', models.OneToOneField(editable=False, on_delete=django.db.models.deletion.CASCADE, to='wagtailcore.Site')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
