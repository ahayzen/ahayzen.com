#!/usr/bin/python3
"""
This file is part of ahayzen.com

Copyright (C) 2016, 2018
    Andrew Hayzen <ahayzen@gmail.com>

ahayzen.com is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

ahayzen.com is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with ahayzen.com.  If not, see <https://www.gnu.org/licenses/>.
"""
from __future__ import unicode_literals

from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db import models
from django.db.models.fields import CharField
from wagtail.admin.edit_handlers import (FieldPanel, PageChooserPanel,
                                                StreamFieldPanel)
from wagtail.core.fields import StreamField
from wagtail.core.models import Page
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.search.index import SearchField
from wagtail.snippets.models import register_snippet

from wagtail.contrib.settings.models import BaseSetting, register_setting

from ahayzen.base.blocks import AhayzenStreamBlock


class AhayzenBodyMixin(models.Model):
    is_abstract = True
    body = StreamField(AhayzenStreamBlock())

    content_panels = [
        StreamFieldPanel("body"),
    ]

    search_fields = [
        SearchField("body", partial_match=True, boost=1),
    ]

    class Meta:
        abstract = True


class AhayzenPageMixin(models.Model):
    is_abstract = True
    subtitle = CharField(blank=False, max_length=250)
    listing_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )

    content_panels = [
        FieldPanel("subtitle", classname="full")
    ]

    promote_panels = [
        ImageChooserPanel("listing_image"),
    ]

    search_fields = [
        SearchField("subtitle", partial_match=True, boost=1),
        # Boost title so it is above body
        # declare after as we are overriding
        SearchField("title", partial_match=True, boost=5),
    ]

    class Meta:
        abstract = True


class AhayzenPaginationMixin(models.Model):
    is_abstract = True
    paginate_by = 15  # TODO: make configurable?

    def get_paginator_results(self, request, items):
        paginator = Paginator(items, self.paginate_by)

        page = request.GET.get('page')

        try:
            results = paginator.page(page)
        except PageNotAnInteger:
            results = paginator.page(1)
        except EmptyPage:
            results = paginator.page(paginator.num_pages)

        return results

    class Meta:
        abstract = True


class AhayzenPage(Page, AhayzenPageMixin):
    is_abstract = True

    class Meta:
        abstract = True

    content_panels = [
        FieldPanel("title", classname="full title"),
    ] + AhayzenPageMixin.content_panels

    promote_panels = [
        FieldPanel('slug'),
    ] + AhayzenPageMixin.promote_panels

    search_fields = Page.search_fields + AhayzenPageMixin.search_fields


class FooterLink(models.Model):
    link = models.ForeignKey(
        'wagtailcore.Page',
        null=True,
        blank=False,
        on_delete=models.SET_NULL,
        related_name='+'
    )

    def __str__(self):
        return self.link.title

    panels = [
        PageChooserPanel("link"),
    ]


register_snippet(FooterLink)


class MenuBarLink(models.Model):
    title = models.CharField(max_length=30)
    url = models.URLField(max_length=1024)

    def __str__(self):
        return self.title

    panels = [
        FieldPanel("title"),
        FieldPanel("url"),
    ]


register_snippet(MenuBarLink)


@register_setting
class KeysSettings(BaseSetting):
    google_site_verification_code = models.CharField(
        max_length=255, help_text='Google Site Verification Code',
        default='', blank=True)
    recaptcha_public_key = models.CharField(
        max_length=255, help_text='ReCaptcha public key',
        default='', blank=True)
    recaptcha_private_key = models.CharField(
        max_length=255, help_text='ReCaptcha private key',
        default='', blank=True)

    panels = [
        FieldPanel("google_site_verification_code"),
        FieldPanel("recaptcha_public_key"),
        FieldPanel("recaptcha_private_key"),
    ]
