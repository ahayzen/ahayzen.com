#!/usr/bin/python3
"""
This file is part of ahayzen.com

Copyright (C) 2016, 2018
    Andrew Hayzen <ahayzen@gmail.com>

ahayzen.com is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

ahayzen.com is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with ahayzen.com.  If not, see <https://www.gnu.org/licenses/>.
"""
from django.db.models.aggregates import Count
from django.shortcuts import render
from django.utils.functional import cached_property
from modelcluster.contrib.taggit import ClusterTaggableManager
from modelcluster.fields import ParentalKey
from taggit.models import TaggedItemBase
from wagtail.admin.edit_handlers import FieldPanel

from ahayzen.base.models import (AhayzenPage, AhayzenBodyMixin,
                                 AhayzenPaginationMixin)


class BlogIndexPage(AhayzenPage, AhayzenBodyMixin, AhayzenPaginationMixin):
    def __init__(self, *args, **kwargs):
        super(BlogIndexPage, self).__init__(*args, **kwargs)

        [f for f in self._meta.fields
         if f.name == "show_in_menus"][0].default = True

    parent_page_types = ["home.HomePage", "section.SectionIndexPage"]
    subpage_types = ["blog.BlogEntryPage"]

    content_panels = (
        AhayzenPage.content_panels + AhayzenBodyMixin.content_panels
    )

    promote_panels = AhayzenPage.promote_panels + [
        FieldPanel('show_in_menus'),
    ]

    search_fields = AhayzenPage.search_fields + AhayzenBodyMixin.search_fields

    @cached_property
    def children(self):
        children = BlogEntryPage.objects.descendant_of(self)

        return children.live().order_by("-first_published_at")

    @cached_property
    def popular_tags(self):
        """Get the most popular live tags"""

        # Get all the BlogEntryTag for the live children
        live_tags = BlogEntryTag.objects.filter(
            content_object__in=self.children
        )

        # Aggregate the tags and order by count
        live_tags_ordered = live_tags.values("tag__name").annotate(
            item_count=Count('tag')
        ).order_by('-item_count')

        # Extract the name from the tags
        return [tag["tag__name"] for tag in live_tags_ordered][:10]

    def serve(self, request):
        children = self.children

        # Filter by tag
        tag = request.GET.get('tag')

        if tag:
            children = children.filter(tags__name=tag)

        paginated_children = self.get_paginator_results(request, children)

        return render(
            request,
            self.template,
            {
                'self': self,
                'children': paginated_children,
                'tag': tag,
            }
        )


class BlogEntryTag(TaggedItemBase):
    content_object = ParentalKey('blog.BlogEntryPage',
                                 related_name='tagged_items')


class BlogEntryPage(AhayzenPage, AhayzenBodyMixin):
    parent_page_types = ["blog.BlogIndexPage"]
    subpage_types = []

    tags = ClusterTaggableManager(through=BlogEntryTag, blank=True)

    content_panels = (
        AhayzenPage.content_panels + AhayzenBodyMixin.content_panels
    )

    promote_panels = AhayzenPage.promote_panels + [
        FieldPanel("tags"),
    ]

    search_fields = AhayzenPage.search_fields + AhayzenBodyMixin.search_fields

    def get_next_live_sibling(self):
        return self.get_next_siblings(
            inclusive=False,
        ).filter(live=True).first()

    def get_previous_live_sibling(self):
        return self.get_prev_siblings(
            inclusive=False,
        ).filter(live=True).first()

    @cached_property
    def parent_index(self):
        return self.get_ancestors().last()
