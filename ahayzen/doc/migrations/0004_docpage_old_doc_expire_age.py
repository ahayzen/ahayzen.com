# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-06-12 08:26
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('doc', '0003_docpage_show_alternative_views'),
    ]

    operations = [
        migrations.AddField(
            model_name='docpage',
            name='old_doc_expire_age',
            field=models.IntegerField(default=365, help_text='After how many days should the page show a warning for an old document', verbose_name='old doc expire age'),
        ),
    ]
