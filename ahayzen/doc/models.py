#!/usr/bin/python3
"""
This file is part of ahayzen.com

Copyright (C) 2016, 2018
    Andrew Hayzen <ahayzen@gmail.com>

ahayzen.com is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

ahayzen.com is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with ahayzen.com.  If not, see <https://www.gnu.org/licenses/>.
"""
import re

from django.db.models.fields import BooleanField, IntegerField
from django.shortcuts import render
from django.utils import timezone
from wagtail.admin.edit_handlers import FieldPanel, MultiFieldPanel

from ahayzen.section.models import SectionPage, SectionIndexPage

HTML_EXTENSION_RE = re.compile(r"(.*)\.html")


class DocPage(SectionPage):
    parent_page_types = [
        "home.HomePage", "doc.DocIndexPage",
    ]
    subpage_types = []

    show_alternative_views = BooleanField(
        verbose_name='show alternative views',
        default=True,
        help_text="Whether to show the alternative views buttons, eg code view"
    )

    show_doc_warning = BooleanField(
        verbose_name='show doc warning',
        default=True,
        help_text="Whether to show the documentation warning"
    )

    show_old_doc_warning = BooleanField(
        verbose_name='show old doc warning',
        default=True,
        help_text="Whether to show a warning that the documentation is old"
    )

    old_doc_expire_age = IntegerField(
        verbose_name='old doc expire age',
        default=365,
        help_text=("After how many days should the page show a warning for an "
                   "old document"),
    )

    promote_panels = SectionPage.promote_panels + [
        MultiFieldPanel([
            FieldPanel("show_alternative_views"),
            FieldPanel("show_doc_warning"),
            FieldPanel("show_old_doc_warning"),
            FieldPanel("old_doc_expire_age"),
        ], 'Doc page configuration'),
    ]

    def __init__(self, *args, **kwargs):
        super(DocPage, self).__init__(*args, **kwargs)

        self.this_template = re.match(HTML_EXTENSION_RE,
                                      self.template).group(1)

    def is_old_doc(self):
        delta = timezone.now() - self.get_latest_revision().created_at

        return (self.show_old_doc_warning and
                delta.days > self.old_doc_expire_age)

    def serve(self, request, *args, **kwargs):
        if request.GET.get("view") == "code":
            return render(
                request,
                self.this_template + "_code_view.html",
                self.get_context(request, *args, **kwargs)
            )
        elif request.GET.get("view") == "markdown":
            return render(
                request,
                self.this_template + "_markdown_view.html",
                self.get_context(request, *args, **kwargs)
            )
        else:
            return super(DocPage, self).serve(request, *args, **kwargs)


class DocIndexPage(SectionIndexPage):
    parent_page_types = [
        "home.HomePage", "doc.DocIndexPage", "section.SectionIndexPage",
    ]
    subpage_types = ["doc.DocIndexPage", "doc.DocPage"]
