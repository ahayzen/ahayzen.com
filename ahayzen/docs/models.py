#!/usr/bin/python3
"""
This file is part of ahayzen.com

Copyright (C) 2016, 2018
    Andrew Hayzen <ahayzen@gmail.com>

ahayzen.com is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

ahayzen.com is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with ahayzen.com.  If not, see <https://www.gnu.org/licenses/>.
"""
from django.conf import settings
from django.core.management import call_command
from django.db.models.fields import CharField
from django.http.response import HttpResponseRedirect, Http404, HttpResponse
from django.shortcuts import render
from django.utils.functional import cached_property
from wagtail.contrib.routable_page.models import RoutablePageMixin, route
from wagtail.admin.edit_handlers import FieldPanel, MultiFieldPanel

import markdown
import os
import re

from ahayzen.base.models import AhayzenPage

# anything [ anything ] ( anything ) anything
LINK_MATCHER = re.compile(r'(.*?)\[(.*?)\]\((.*?)\)(.*?)')


def patch_link(path, base_url):
    if path.startswith("http://") or path.startswith("https://"):
        return path
    elif path.endswith(".md"):
        path = path[:-3].strip("/")
    else:
        path = path.strip("/")

    return os.path.join(base_url, path)


def pre_process_links(data, base_url):
    lines = data.split("\n")
    output = []

    for line in lines:
        match = re.match(LINK_MATCHER, line)

        if match:
            groups = list(match.groups())
            groups[2] = patch_link(groups[2], base_url)

            output.append(
                groups[0] + "[" + groups[1] + "](" +
                groups[2] + ")" + groups[3]
            )
        else:
            output.append(line)

    return "\n".join(output)


def post_process_styling(data):
    lines = data.split("\n")
    output = []

    for line in lines:
        line = line.replace("<p><code>",
                            "<section class=\"block-code\"><pre><code>")
        line = line.replace("<h3>", "<h5>")
        line = line.replace("<h2>", "<h4>")
        line = line.replace("<h1>", "<h3>")

        line = line.replace("</code></p>", "</code></pre></section>")
        line = line.replace("</h3>", "</h5>")
        line = line.replace("</h2>", "</h4>")
        line = line.replace("</h1>", "</h3>")

        output.append(line)

    return "\n".join(output)


def markdown_html(data, base_url):
    data = pre_process_links(data, base_url)

    data = markdown.markdown(data)

    data = post_process_styling(data)

    return data

# TODO: DocEntry(Model)  dir  url  file  identifier
# TODO: DocDir(Model)  parent url  identifier
# TODO: change to MarkdownSectionPage, MarkdownEntry, MarkdownDir


class DocsPage(RoutablePageMixin, AhayzenPage):
    def __init__(self, *args, **kwargs):
        super(AhayzenPage, self).__init__(*args, **kwargs)

        [f for f in self._meta.fields
         if f.name == "show_in_menus"][0].default = True

    parent_page_types = [
        "home.HomePage", "section.SectionIndexPage",
    ]
    subpage_types = []

    identifier = CharField(
        blank=False, max_length=128, unique=True,  # TODO: check unique works
        help_text="Used as the folder name and ID for hooks"
    )
    git_url = CharField(blank=False, max_length=255)

    token_header_key = CharField(blank=True, default="HTTP_X_GITLAB_TOKEN",
                                 max_length=255)
    token = CharField(blank=True, max_length=255)

    content_panels = AhayzenPage.content_panels + [
        FieldPanel("git_url"),
        FieldPanel("identifier"),
        MultiFieldPanel([
            FieldPanel("token"),
            FieldPanel("token_header_key"),
        ], "Secret Webhook Token"),
    ]

    promote_panels = AhayzenPage.promote_panels + [
        FieldPanel('show_in_menus'),
    ]

    @cached_property
    def get_base_dir(self):
        directory = getattr(settings, "DOCS_GIT_CLONE_DIR", "/tmp")
        return os.path.join(directory, self.identifier)

    def get_markdown_dir(self, directory):
        directory = os.path.join(self.get_base_dir, directory)

        dirs = []
        files = []

        if os.path.exists(directory):
            for path in os.listdir(directory):
                path = os.path.join(directory, path)

                if os.path.isfile(path) and path.endswith(".md"):
                    files.append(path)
                elif os.path.isdir(path) and not path.startswith("."):
                    dirs.append(path)

            return dirs, files, self.get_base_dir
        else:
            raise Http404()

    def get_markdown_page(self, path):
        doc = os.path.join(self.get_base_dir, path)

        if os.path.exists(doc):
            return markdown_html(open(doc).read(), self.url)
        else:
            return None

    def get_path_from_request(self, request):
        return request.path[len(self.url):].strip("/")

    def get_sitemap_urls(self, request=None):
        paths = []

        # Build a list of the paths for markdown dirs and files
        for root, dirs, files in os.walk(self.get_base_dir):
            base = root[len(self.get_base_dir):].strip("/")

            if ".git" not in root:
                paths.extend(
                    [
                        os.path.join(base, dir)
                        for dir in dirs
                        if not dir.startswith(".")
                    ]
                )
                paths.extend(
                    [
                        os.path.join(base, file)[:-3]
                        for file in files
                        if file.endswith(".md")
                    ]
                )

        return [
            {
                "location": os.path.join(self.full_url, path)
            }
            for path in paths
        ]

    def is_markdown_directory(self, path):
        path = os.path.join(self.get_base_dir, path)

        return os.path.exists(path) and os.path.isdir(path)

    def is_markdown_file(self, path):
        path = os.path.join(self.get_base_dir, path + ".md")

        return os.path.exists(path) and os.path.isfile(path)

    def make_dir_markdown(self, dirs, files, base_dir):
        data = []

        if len(dirs) > 0:
            data.append("# Directories")

            for d in sorted(dirs):
                name = d.split("/")[-1]
                data.append("  * [" + name + "](" + d[len(base_dir):] + ")")

        if len(files) > 0:
            data.append("# Files")

            for file in sorted(files):
                name = file.split("/")[-1]
                data.append("  * [" + name + "](" + file[len(base_dir):] + ")")

        return markdown_html("\n".join(data), self.url)

    @route(r'^$')
    def markdown_index(self, request):
        """Root page redirect to README.md"""

        return HttpResponseRedirect(
            self.url + self.reverse_subpage(
                "markdown-resolve", args=("README",)
            )
        )

    @route(r'^(.)+$', name="markdown-resolve")
    def markdown_resolve(self, request, page):
        path = self.get_path_from_request(request)

        # Check this isn't an update call
        if path == "update":
            return self.update(request)

        parts = path.split("/")

        # Get parents of this page
        parents = [
            parent
            for parent in self.get_ancestors(inclusive=True)
            if not parent.is_root() and parent.depth > 2
        ]

        # Generate parents from markdown path
        for i in range(len(parts) - 1):
            parents.append({
                "title": parts[i],
                "url": self.url + self.reverse_subpage(
                    "markdown-resolve", args=("/".join(parts[:i + 1]),)
                )
            })

        # Load markdown for file or dir
        if self.is_markdown_file(path):
            data = self.get_markdown_page(path + ".md")
            template = "docs/docs_page.html"
        elif self.is_markdown_directory(path):
            data = self.make_dir_markdown(*self.get_markdown_dir(path))
            template = "docs/docs_dir.html"
        else:
            raise Http404()

        return render(request, template, {
            "formatted_markdown": data,
            "path": path,
            "parents": parents,
            "subtitle": self.subtitle,
            "title": parts[-1],
        })

    def update(self, request):
        if request.META.get(self.token_header_key, "") == self.token:
            call_command('pull_doc', self.identifier)
            return HttpResponse(content="OK", status=200)
        else:
            raise Http404
