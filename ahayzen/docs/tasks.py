#!/usr/bin/python3
"""
This file is part of ahayzen.com

Copyright (C) 2016, 2018
    Andrew Hayzen <ahayzen@gmail.com>

ahayzen.com is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

ahayzen.com is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with ahayzen.com.  If not, see <https://www.gnu.org/licenses/>.
"""
from django.conf import settings

import os
import subprocess

from ahayzen.docs.models import DocsPage


def pull_doc(identifier):
    doc_page = DocsPage.objects.get(identifier=identifier)
    base_dir = getattr(settings, "DOCS_GIT_CLONE_DIR", "/tmp")
    doc_dir = os.path.join(base_dir, doc_page.identifier)

    if not os.path.exists(doc_dir):
        p = subprocess.Popen(
            ["git", "clone", doc_page.git_url, doc_page.identifier],
            cwd=base_dir,
            stderr=subprocess.PIPE,
            stdout=subprocess.PIPE,
        )
    else:
        p = subprocess.Popen(
            ["git", "pull", doc_page.git_url],
            cwd=doc_dir,
            stderr=subprocess.PIPE,
            stdout=subprocess.PIPE,
        )

    print(p.stdout.read())
    print(p.returncode)
