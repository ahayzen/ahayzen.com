#!/usr/bin/python3
"""
This file is part of ahayzen.com

Copyright (C) 2016, 2018
    Andrew Hayzen <ahayzen@gmail.com>

ahayzen.com is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

ahayzen.com is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with ahayzen.com.  If not, see <https://www.gnu.org/licenses/>.
"""

from django.contrib.syndication.views import Feed

from ahayzen.blog.models import BlogEntryPage

FEED_LENGTH = 9


class GenericFeed(Feed):
    link = ""
    model = None

    def get_object(self, request, *args, **kwargs):
        self.link = request.get_full_path()
        return None

    def items(self):
        return self.model.order_by('-first_published_at')[:FEED_LENGTH]

    def item_description(self, item):
        return self.get_body_text(item)

    # def item_enclosure_url(self, item):
    #     rendition = self.item_thumbnail_rendition(item)
    #
    #     if rendition:
    #         return rendition.url
    #
    # def item_enclosure_length(self, item):
    #     rendition = self.item_thumbnail_rendition(item)
    #
    #     if rendition:
    #         return rendition.file.size
    #
    # def item_enclosure_mime_type(self, item):
    #     rendition = self.item_thumbnail_rendition(item)
    #
    #     if rendition:
    #         return mimetypes.guess_type(rendition.file.name)[0]

    def item_link(self, item):
        return item.url

    def item_pubdate(self, item):
        return item.first_published_at

    # def item_thumbnail_image(self, item):
    #     if hasattr(item, "listing_image"):
    #         return item.listing_image
    #     else:
    #         return None
    #
    # def item_thumbnail_rendition(self, item):
    #     image = self.item_thumbnail_image(item)
    #
    #     if image:
    #         return image.get_rendition('width-300')
    #     else:
    #         return None

    def item_title(self, item):
        return item.title


class BlogLatestFeed(GenericFeed):
    model = BlogEntryPage.objects.live()
    title = "Latest blog feed"
    link = "/feeds/blog/latest/"
    description = "Latest blog entries"

    def get_body_text(self, item):
        return item.body


class BlogTagFeed(BlogLatestFeed):
    title = "Blog entries for a tag"
    description = "Blog entries for a tag"

    def get_object(self, request, tag, *args, **kwargs):
        super(BlogTagFeed, self).get_object(request, tag)

        self.title = "Blog entries for the tag: " + tag
        return tag

    def items(self, tag):
        objects = self.model.filter(tags__name=tag)
        return objects.order_by('-first_published_at')[:FEED_LENGTH]
