#!/usr/bin/python3
"""
This file is part of ahayzen.com

Copyright (C) 2016, 2018
    Andrew Hayzen <ahayzen@gmail.com>

ahayzen.com is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

ahayzen.com is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with ahayzen.com.  If not, see <https://www.gnu.org/licenses/>.
"""
from django.conf.urls import url
from django.views.generic.base import RedirectView

from ahayzen.feed.feeds import BlogLatestFeed, BlogTagFeed

urlpatterns = [
    url(r'^blog/latest/$', BlogLatestFeed()),
    url(r'^blog/tag/(?P<tag>[A-z0-9]+)/$', BlogTagFeed()),
    url(r'^blog/$',
        RedirectView.as_view(url='/feeds/blog/latest', permanent=False))
]
