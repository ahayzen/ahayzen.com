#!/usr/bin/python3
"""
This file is part of ahayzen.com

Copyright (C) 2016, 2018
    Andrew Hayzen <ahayzen@gmail.com>

ahayzen.com is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

ahayzen.com is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with ahayzen.com.  If not, see <https://www.gnu.org/licenses/>.
"""
from captcha.fields import ReCaptchaField
from modelcluster.fields import ParentalKey
from wagtail.admin.edit_handlers import (
    FieldPanel, InlinePanel, MultiFieldPanel, StreamFieldPanel
)
from wagtail.core.models import Site
from wagtail.core.fields import StreamField
from wagtail.contrib.forms.models import AbstractFormField
from wagtail.search.index import SearchField

from wagtailcaptcha.forms import WagtailCaptchaFormBuilder
from wagtailcaptcha.models import WagtailCaptchaEmailForm

from ahayzen.base.blocks import AhayzenStreamBlock
from ahayzen.base.models import AhayzenPageMixin, KeysSettings


class FormField(AbstractFormField):
    page = ParentalKey('FormPage', related_name='form_fields')


# Custom FormBuilder so that we can override public and private key
# from database rather than environment
#
# https://github.com/springload/wagtail-django-recaptcha/blob/
#   master/wagtailcaptcha/forms.py#L15
class FormBuilder(WagtailCaptchaFormBuilder):
    @property
    def formfields(self):
        # Use the default site to retrieve the recaptcha keys
        # as we have no request here
        keys = KeysSettings.for_site(Site.objects.get(is_default_site=True))

        # Add wagtailcaptcha to formfields property
        fields = super(WagtailCaptchaFormBuilder, self).formfields
        fields[self.CAPTCHA_FIELD_NAME] = ReCaptchaField(
            label='',
            public_key=keys.recaptcha_public_key,
            private_key=keys.recaptcha_private_key,
        )

        return fields


class FormPage(WagtailCaptchaEmailForm, AhayzenPageMixin):
    form_builder = FormBuilder

    def __init__(self, *args, **kwargs):
        super(FormPage, self).__init__(*args, **kwargs)

        [f for f in self._meta.fields
         if f.name == "show_in_menus"][0].default = True

    description = StreamField(AhayzenStreamBlock(), blank=True)
    landing_page_text = StreamField(AhayzenStreamBlock(), blank=True)

    # TODO: should be body and blank=False

    content_panels = [
        FieldPanel("title", classname="full title"),
    ] + AhayzenPageMixin.content_panels + [
        StreamFieldPanel("description"),
        StreamFieldPanel("landing_page_text"),
        InlinePanel('form_fields', label="Form fields"),  # fields in the form
        MultiFieldPanel([
            FieldPanel('to_address'),
            FieldPanel('from_address'),
            FieldPanel('subject'),
        ], "Email")
    ]

    promote_panels = AhayzenPageMixin.promote_panels + [
        FieldPanel("slug"),
        FieldPanel("show_in_menus"),
    ]

    search_fields = (
        list(WagtailCaptchaEmailForm.search_fields) +
        AhayzenPageMixin.search_fields + [
            SearchField("description", partial_match=True, boost=1),
            SearchField("landing_page_text", partial_match=True, boost=1),
        ]
    )
