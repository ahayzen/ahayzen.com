# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-05-17 12:21
from __future__ import unicode_literals

from django.db import migrations
import wagtail.core.blocks
import wagtail.core.fields


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0004_auto_20160517_0404'),
    ]

    operations = [
        migrations.AddField(
            model_name='homepage',
            name='links',
            field=wagtail.core.fields.StreamField((('link', wagtail.core.blocks.PageChooserBlock(icon='link')),), blank=True),
        ),
    ]
