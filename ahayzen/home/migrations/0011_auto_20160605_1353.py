# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-06-05 13:53
from __future__ import unicode_literals

from django.db import migrations
import wagtail.contrib.table_block.blocks
import wagtail.core.blocks
import wagtail.core.fields
import wagtail.documents.blocks
import wagtail.embeds.blocks
import wagtail.images.blocks


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0010_auto_20160605_1323'),
    ]

    operations = [
        migrations.AlterField(
            model_name='homepage',
            name='body',
            field=wagtail.core.fields.StreamField((('heading', wagtail.core.blocks.CharBlock(form_classname='full title', icon='title')), ('document', wagtail.documents.blocks.DocumentChooserBlock(icon='doc-empty')), ('embed', wagtail.embeds.blocks.EmbedBlock(icon='media')), ('external_link', wagtail.core.blocks.StructBlock((('title', wagtail.core.blocks.CharBlock(required=True)), ('url', wagtail.core.blocks.URLBlock(required=True)), ('description', wagtail.core.blocks.RichTextBlock(required=True)), ('image', wagtail.images.blocks.ImageChooserBlock(required=False))), icon='site')), ('image', wagtail.images.blocks.ImageChooserBlock(icon='image')), ('link', wagtail.core.blocks.PageChooserBlock(icon='link')), ('paragraph', wagtail.core.blocks.RichTextBlock(icon='pilcrow')), ('raw_html', wagtail.core.blocks.RawHTMLBlock()), ('table', wagtail.contrib.table_block.blocks.TableBlock(icon='table')), ('code', wagtail.core.blocks.TextBlock(icon='code')), ('latest_block_entries', wagtail.core.blocks.StructBlock((('amount', wagtail.core.blocks.CharBlock(help_text='Amount of blog entries to show', required=False)),), icon='pick')))),
        ),
    ]
