#!/usr/bin/python3
"""
This file is part of ahayzen.com

Copyright (C) 2016, 2018
    Andrew Hayzen <ahayzen@gmail.com>

ahayzen.com is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

ahayzen.com is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with ahayzen.com.  If not, see <https://www.gnu.org/licenses/>.
"""
from __future__ import unicode_literals

from wagtail.admin.edit_handlers import StreamFieldPanel
from wagtail.core.fields import StreamField
from wagtail.search.index import SearchField

from ahayzen.base.blocks import AhayzenStreamHomeBlock
from ahayzen.base.models import AhayzenPage


class HomePage(AhayzenPage):
    parent_page_types = []
    subpage_types = [
        "blog.BlogIndexPage", "doc.DocIndexPage", "doc.DocPage",
        "docs.DocsPage",
        "form.FormPage", "section.SectionIndexPage", "section.SectionPage",
    ]

    body = StreamField(AhayzenStreamHomeBlock())

    content_panels = AhayzenPage.content_panels + [
        StreamFieldPanel("body"),
    ]

    search_fields = AhayzenPage.search_fields + [
        SearchField("body", partial_match=True, boost=1),
    ]
