#!/usr/bin/python3
"""
This file is part of ahayzen.com

Copyright (C) 2016, 2018
    Andrew Hayzen <ahayzen@gmail.com>

ahayzen.com is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

ahayzen.com is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with ahayzen.com.  If not, see <https://www.gnu.org/licenses/>.
"""
from __future__ import unicode_literals

from django.db import models
from django.shortcuts import render
from django.utils.functional import cached_property
from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel
from wagtail.core.fields import StreamField
from wagtail.search.index import SearchField

from ahayzen.base.blocks import AhayzenStreamBlock
from ahayzen.base.models import (AhayzenPage, AhayzenBodyMixin,
                                 AhayzenPaginationMixin)


class SectionPage(AhayzenPage, AhayzenBodyMixin):
    def __init__(self, *args, **kwargs):
        super(SectionPage, self).__init__(*args, **kwargs)

        [f for f in self._meta.fields
         if f.name == "show_in_menus"][0].default = True

    subpage_types = []
    parent_page_types = ["home.HomePage", "section.SectionIndexPage"]

    content_panels = (
        AhayzenPage.content_panels + AhayzenBodyMixin.content_panels
    )

    promote_panels = AhayzenPage.promote_panels + [
        FieldPanel('show_in_menus'),
    ]

    search_fields = AhayzenPage.search_fields + AhayzenBodyMixin.search_fields


class SectionIndexPage(AhayzenPage, AhayzenPaginationMixin):
    def __init__(self, *args, **kwargs):
        super(SectionIndexPage, self).__init__(*args, **kwargs)

        [f for f in self._meta.fields
         if f.name == "show_in_menus"][0].default = True

    parent_page_types = ["home.HomePage", "section.SectionIndexPage"]
    subpage_types = [
        "doc.DocIndexPage", "form.FormPage", "section.SectionPage",
        "section.SectionIndexPage",
    ]

    alphabetical_order = models.BooleanField(default=True)
    description = StreamField(AhayzenStreamBlock())

    content_panels = AhayzenPage.content_panels + [
        FieldPanel("alphabetical_order"),
        StreamFieldPanel("description"),
    ]

    promote_panels = AhayzenPage.promote_panels + [
        FieldPanel('show_in_menus'),
    ]

    search_fields = AhayzenPage.search_fields + [
        SearchField("description", partial_match=True, boost=1),
    ]

    @cached_property
    def children(self):
        children = self.get_children().live()

        if self.alphabetical_order:
            children = children.order_by("title")

        return children

    def serve(self, request):
        paginated_children = self.get_paginator_results(request, self.children)

        return render(
            request,
            self.template,
            {
                'self': self,
                'children': paginated_children,
            }
        )
