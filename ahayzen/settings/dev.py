from .base import *


# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ["*"]

for template_engine in TEMPLATES:
    template_engine['OPTIONS']['debug'] = True


INSTALLED_APPS += [
    'wagtail.contrib.styleguide',
]


# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'changeme!'


EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

DOCS_GIT_CLONE_DIR = "/tmp"


try:
    from .local import *
except ImportError:
    pass
