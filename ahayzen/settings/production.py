from .base import *

# https://github.com/torchbox/cookiecutter-wagtail/blob/master/%7B%7Bcookiecutter.repo_name%7D%7D/%7B%7Bcookiecutter.repo_name%7D%7D/settings/production.py

ALLOWED_HOSTS = ["*"]

DEBUG = False

# minify CSS
COMPRESS_CSS_FILTERS = [
    'compressor.filters.cssmin.CSSMinFilter',
]

RECAPTCHA_PUBLIC_KEY = "MyRecaptchaKey123"
RECAPTCHA_PRIVATE_KEY = "MyRecaptchaPrivateKey456"

# Production paths
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': "/srv/webapp/db/db.sqlite3",
    }
}

STATIC_ROOT = "/srv/webapp/static/"
MEDIA_ROOT = "/srv/webapp/media/"

BASE_URL = "http://www.ahayzen.com/"
DOCS_GIT_CLONE_DIR = "/srv/webapp/docs"

try:
    from .local import *
except ImportError:
    pass
