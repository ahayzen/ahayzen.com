#!/usr/bin/python3
"""
This file is part of ahayzen.com

Copyright (C) 2016, 2018
    Andrew Hayzen <ahayzen@gmail.com>

ahayzen.com is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

ahayzen.com is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with ahayzen.com.  If not, see <https://www.gnu.org/licenses/>.
"""
from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic.base import TemplateView
from wagtail.admin import urls as wagtailadmin_urls
from wagtail.core import urls as wagtail_urls
from wagtail.documents import urls as wagtaildocs_urls

from wagtail.contrib.sitemaps.views import sitemap

from ahayzen.feed import urls as feed_urls

urlpatterns = [
    url(r'^django-admin/', admin.site.urls),

    url(r'^admin/', include(wagtailadmin_urls)),
    url(r'^documents/', include(wagtaildocs_urls)),

    url(r'^feeds/', include(('ahayzen.feed.urls', 'feeds'), namespace='feeds')),

    url(r'^sitemap\.xml$', sitemap),

    url(r'^test403/$', TemplateView.as_view(template_name="403.html")),
    url(r'^test404/$', TemplateView.as_view(template_name="404.html")),
    url(r'^test500/$', TemplateView.as_view(template_name="500.html")),

    url(r'', include(wagtail_urls)),
]


if settings.DEBUG:
    from django.conf.urls.static import static
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns
    # from django.views.generic import TemplateView

    # Serve static and media files from development server
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
