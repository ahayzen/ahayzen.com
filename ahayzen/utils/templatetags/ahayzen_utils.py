#!/usr/bin/python3
"""
This file is part of ahayzen.com

Copyright (C) 2016, 2018
    Andrew Hayzen <ahayzen@gmail.com>

ahayzen.com is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

ahayzen.com is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with ahayzen.com.  If not, see <https://www.gnu.org/licenses/>.
"""
from django import template
from wagtail.core.models import Site

from ahayzen.base.models import KeysSettings, MenuBarLink, FooterLink
from ahayzen.blog.models import BlogEntryPage

register = template.Library()


@register.filter
def get_class(obj):
    return obj.__class__.__name__


@register.simple_tag
def get_latest_blog_entries(amount=2):
    try:
        amount = int(amount)
    except ValueError:
        amount = 2

    pages = BlogEntryPage.objects.live()
    return pages.order_by("-first_published_at")[:amount]


@register.simple_tag
def get_footer_item_snippets():
    return FooterLink.objects.all()


@register.simple_tag(takes_context=True)
def get_google_site_verification_code(context):
    return KeysSettings.for_site(Site.find_for_request(
        context["request"])).google_site_verification_code


@register.simple_tag(takes_context=True)
def get_menu_items(context):
    children = get_root_page(context).get_children().live()

    return children.filter(show_in_menus=True)


@register.simple_tag
def get_menu_item_snippets():
    return MenuBarLink.objects.all()


@register.simple_tag(takes_context=True)
def get_root_page(context):
    return Site.find_for_request(context['request']).root_page
