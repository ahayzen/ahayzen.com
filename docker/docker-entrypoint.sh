#!/bin/bash

# Copyright 2020 Andrew Hayzen <ahayzen@gmail.com>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

if [ "$1" = "publish_scheduled_pages" ]; then
    python3 manage.py publish_scheduled_pages
else
    # Perform collectstatic and compress of django data
    python3 "/srv/webapp/ahayzen.com/manage.py" collectstatic --no-input
    python3 "/srv/webapp/ahayzen.com/manage.py" compress

    # Ensure that database has been migrated
    python3 manage.py migrate --no-input

    # Setup a superuser if one doesn't exist yet
    ADMINCOUNT=$(python3 manage.py shell -c "from django.contrib.auth.models import User; print(User.objects.filter(username='admin').count())")

    if [ "$ADMINCOUNT" != "1" ]; then
        echo "Going to create initial admin account"

        # Make super user account
        # python3 manage.py createsuperuser --no-input --username admin
        python3 manage.py shell -c "from django.contrib.auth.models import User; User.objects.create_superuser('admin', 'ahayzen@gmail.com', 'changeme')"
    fi

    exec "$@"
fi
