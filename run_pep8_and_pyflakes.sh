#!/bin/sh

# Absolute path to this script, e.g. /home/user/bin/foo.sh
SCRIPT=$(readlink -f "$0")
# Absolute path this script is in, thus /home/user/bin
SCRIPTPATH=$(dirname "$SCRIPT")

cd $SCRIPTPATH

echo "Running PEP8 and pyflakes over $SCRIPTPATH"

pep8 . --exclude=static,migrations
pyflakes . | grep -v "./ahayzen/settings/"
